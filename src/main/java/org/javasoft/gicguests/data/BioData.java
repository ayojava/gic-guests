package org.javasoft.gicguests.data;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

@Data
public class BioData {

    private String surname;

    private String otherNames;

    private String gender;

    private String maritalStatus;

    private String phoneNo;

    private String altPhoneNo;

    private String email;

    private String altEmail;

    private Date birthday;

    private String ageRange;

    private String contactAddress;

    private String nearestBstp;

    private String category;//first-Timer or new Convert
}
