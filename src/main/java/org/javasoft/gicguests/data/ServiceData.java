package org.javasoft.gicguests.data;

import lombok.Data;

@Data
public class ServiceData {

    private String id;

    private String serviceName;

    private String serviceType;
}
