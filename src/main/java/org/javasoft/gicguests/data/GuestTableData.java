package org.javasoft.gicguests.data;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GuestTableData {

    private String guestId;

    private String fullName;

    private String serviceName;

    private String serviceType;

    private String serviceDate;

    private String ageRange;

    private String category;//first-Timer or new Convert

    private String gender;

    private String maritalStatus;

    private String phoneNo;

    private String otherPhoneNo;

    private String email;

    private String otherEmail;

    private String birthday;

    private String contactAddress;

    private String nearestBstop;

    private String prayerRequest1;

    private String prayerRequest2;

    private String prayerRequest3;

    private String prayerRequest4;

}