package org.javasoft.gicguests.data;

import lombok.Data;
import lombok.val;

@Data
public class PrayerData {

    private String prayerRequest1;

    private String prayerRequest2;

    private String prayerRequest3;

    private String prayerRequest4;

    public String toClearString(){
        val builder = new StringBuilder();
        return builder.append(prayerRequest1).append(prayerRequest2)
                .append(prayerRequest3).append(prayerRequest4).toString();
    }
}
