package org.javasoft.gicguests.data;

import lombok.Data;

import java.util.Date;

@Data
public class ProgramData {

    private String serviceName;

    private String serviceType;

    private Date programDate;

}
