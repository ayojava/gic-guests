package org.javasoft.gicguests.bean.core;

import lombok.val;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

import static org.javasoft.gicguests.utils.ProfileResourceIntf.*;

@Named("navigatorBean")
@SessionScoped
public class NavigatorBean implements Serializable {

    private String selectedPagePath;

    private String selectedMenuPath;

    private String selectedHeaderPath;

    private String selectedFooterPath;

    @Inject
    private UserSessionBean userSessionBean;

    @PostConstruct
    public void init(){

    }

    public void setHomePath() {
        final val builder = new StringBuilder();
        setSelectedPagePath(builder.append(DEFAULT_INCLUDE_PATH)
                .append(DEFAULT_HOME_PATH).append(PAGE_EXTENSION).toString());
    }

    public void setPagePath(String pagePath){
        final val builder = new StringBuilder();
        setSelectedPagePath(builder.append(DEFAULT_INCLUDE_PATH)
                .append(pagePath).append(PAGE_EXTENSION).toString());
    }

    public String getSelectedMenuPath() {
        if (StringUtils.isBlank(selectedMenuPath)) {
            final val builder = new StringBuilder();
            selectedMenuPath = builder
                    .append(DEFAULT_MENU_PATH).append(PAGE_EXTENSION).toString();
        }
        return selectedMenuPath;
    }

    public String getSelectedHeaderPath(){
        if (StringUtils.isBlank(selectedHeaderPath)) {
            final val builder = new StringBuilder();
            selectedHeaderPath = builder
                    .append(DEFAULT_HEADER_PATH).append(PAGE_EXTENSION).toString();
        }
        return selectedHeaderPath;
    }

    public String getSelectedFooterPath(){
        if (StringUtils.isBlank(selectedFooterPath)) {
            final val builder = new StringBuilder();
            selectedFooterPath = builder
                    .append(DEFAULT_FOOTER_PATH).append(PAGE_EXTENSION).toString();
        }
        return selectedFooterPath;
    }

    public String getSelectedPagePath() {
        if (StringUtils.isBlank(selectedPagePath)) {
            setHomePath();
        }
        return selectedPagePath;
    }

    public void setSelectedPagePath(String selectedPagePath) {
        this.selectedPagePath = selectedPagePath;
    }

    public void setSelectedHeaderPath(String selectedHeaderPath){
        this.selectedHeaderPath = selectedHeaderPath;
    }

    public void setSelectedFooterPath(String selectedFooterPath){
        this.selectedFooterPath = selectedFooterPath;
    }
}
