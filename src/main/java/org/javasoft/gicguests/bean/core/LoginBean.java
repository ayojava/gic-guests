package org.javasoft.gicguests.bean.core;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.gicguests.bean.utils.MessageUtilBean;
import org.javasoft.gicguests.service.LoginService;
import org.omnifaces.util.Messages;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Slf4j
@RequestScoped
@Named("loginBean")
public class LoginBean {

    @Getter   @Setter
    private String userName;

    @Getter   @Setter
    private String password;

    @Inject
    private LoginService loginService;

    @Inject
    private MessageUtilBean messageUtilBean;

    public String login() {
        log.info("Username :: {}", userName);
        log.info("Password :: {}", password);
        if(loginService.login(userName,password)){
            Messages.addFlashGlobalInfo(messageUtilBean.getViewMsg());
            return "home?faces-redirect=true";
        }

        Messages.addGlobalError(messageUtilBean.getViewMsg());
        return null;
    }
}
