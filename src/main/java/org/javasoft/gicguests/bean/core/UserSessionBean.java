package org.javasoft.gicguests.bean.core;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.gicguests.payload.systemuser.SystemUserResponse;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Slf4j
@Named("userSessionBean")
@SessionScoped
public class UserSessionBean implements Serializable {

    @Getter @Setter
    private SystemUserResponse systemUserResponse;

    @Getter @Setter
    private String sessionID;

    @PostConstruct
    public void init(){

    }
}
