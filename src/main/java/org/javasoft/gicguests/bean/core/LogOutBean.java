package org.javasoft.gicguests.bean.core;


import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.IOException;

@Slf4j
@Named("logOutBean")
@RequestScoped
public class LogOutBean {

    public void logOut() throws IOException {
        Faces.invalidateSession();
        Faces.redirect("login.faces");
    }
}
