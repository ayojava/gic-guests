package org.javasoft.gicguests.bean.core;

import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Slf4j
@Named
@RequestScoped
public class PagePathBean {

    @Inject
    private NavigatorBean navigatorBean;

    public void setPagePath(String defaultFolder ,String pagePath) {
        navigatorBean.setPagePath(defaultFolder+pagePath);
    }

    public void setHomePath(){
        navigatorBean.setHomePath();
    }
}
