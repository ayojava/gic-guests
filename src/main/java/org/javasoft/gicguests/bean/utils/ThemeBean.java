package org.javasoft.gicguests.bean.utils;


import lombok.Getter;
import lombok.Setter;
import org.omnifaces.cdi.Eager;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;

@Eager
@Named
@ApplicationScoped
public class ThemeBean implements Serializable {

    @Getter @Setter
    private String theme;

    @Getter @Setter
    private String layoutStyleClass;

    @Getter @Setter
    private boolean lightMenu ;

    @PostConstruct
    public void init(){
        theme = "indigo-purple";
        //theme = "teal-yellow";
        layoutStyleClass = "layout-slim layout-light";
        lightMenu = true;
    }
}
