package org.javasoft.gicguests.bean.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Slf4j
@RequestScoped
@Named("messageUtilBean")
public class MessageUtilBean {

    @Getter  @Setter
    private String viewMsg;

    @Getter  @Setter
    private boolean responseStatus;
}
