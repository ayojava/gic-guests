package org.javasoft.gicguests.bean.excel;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.javasoft.gicguests.bean.guests.GuestPageBean;
import org.omnifaces.util.Faces;

import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@RequestScoped
@Named("guestTableExcelBean")
public class GuestTableExcelBean {

    @Inject
    private GuestPageBean guestPageBean;

    private String[] agentReportColumns =
            {"No","Full Name","Gender","Marital Status","Phone No","Other Phone No",
            "Email","Other Email" ,"Birthday" ,"Age Range","Contact Address","Nearest B/Stop" ,"Prayer Request 1",
            "Prayer Request 2" ,"Prayer Request 3" , "Prayer Request 4" ,"Service Type " ,"Service Name",
            "Date " ,"Category"};

    public void generateExcelSheet(){
        log.info("Came here -----------------------1");
        final val workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Test ");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for(int i = 0; i < agentReportColumns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(agentReportColumns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with employees data
        int rowNum = 1 , serialNo = 1;
        /*
        private String[] agentReportColumns =
                {"No","Full Name","Gender","Marital Status","Phone No","Other Phone No",
                        "Email","Other Email" ,"Birthday" ,"Age Range","Contact Address","Nearest B/Stop" ,"Prayer Request 1",
                        "Prayer Request 2" ,"Prayer Request 3" , "Prayer Request 4" ,"Service Type " ,"Service Name",
                        "Date " ,"Category"};
        */

        final val guestsData = guestPageBean.getGuestsData();
        for(val aGuestData : guestsData){
            Row row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(rowNum);
            row.createCell(1).setCellValue(aGuestData.getFullName());
            row.createCell(2).setCellValue(aGuestData.getGender());
            row.createCell(3).setCellValue(aGuestData.getMaritalStatus());
            row.createCell(4).setCellValue(aGuestData.getPhoneNo());
            row.createCell(5).setCellValue(aGuestData.getOtherPhoneNo());
            row.createCell(6).setCellValue(aGuestData.getEmail());
            row.createCell(7).setCellValue(aGuestData.getOtherEmail());
            row.createCell(8).setCellValue(aGuestData.getBirthday());
            row.createCell(9).setCellValue(aGuestData.getAgeRange());
            row.createCell(10).setCellValue(aGuestData.getContactAddress());
            row.createCell(11).setCellValue(aGuestData.getNearestBstop());
            row.createCell(12).setCellValue(aGuestData.getPrayerRequest1());
            row.createCell(13).setCellValue(aGuestData.getPrayerRequest2());
            row.createCell(14).setCellValue(aGuestData.getPrayerRequest3());
            row.createCell(15).setCellValue(aGuestData.getPrayerRequest4());
            row.createCell(16).setCellValue(aGuestData.getServiceType());
            row.createCell(17).setCellValue(aGuestData.getServiceName());
            row.createCell(18).setCellValue(aGuestData.getServiceDate());
            row.createCell(19).setCellValue(aGuestData.getCategory());
            rowNum++;
            serialNo++;
        }


        for(int i = 0; i < agentReportColumns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        val sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        final val dateFormat = sdf.format(new Date());
        String filename = "download-"+dateFormat+".xlsx";
        HttpServletResponse response = Faces.getResponse();
        response.reset();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");

        try {
            log.info("Came here -------------------- 2");
            final val outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
            workbook.close();
        } catch (Exception ex) {
            log.error("Exception :::: {}",ex);
        }
        Faces.responseComplete();
    }

//    public void postProcessXLS(Object document) {
//        HSSFWorkbook workbook = (HSSFWorkbook) document;
//        HSSFSheet sheet = workbook.getSheetAt(0);
//
//        HSSFRow header = sheet.getRow(0);
//        HSSFCellStyle cellStyle = workbook.createCellStyle();
//        cellStyle.setFillForegroundColor(HSSFColor.GREEN.index);
//        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//
//        log.info("Header Columns Count :::: {}" , header.getPhysicalNumberOfCells());
//        for(int i=0; i < header.getPhysicalNumberOfCells();i++){
//            log.info("Header Cell Names :::: {}",header.getCell(i).getStringCellValue());
//        }
//
//        log.info("Guest Data Size :::: {}" , guestPageBean.getGuestsData().size());
//    }
}


//https://gist.github.com/baybatu/bfead9eb6a681727fc97
//http://lauraliparulo.altervista.org/primefaces-4-0-add-column-excel-report/