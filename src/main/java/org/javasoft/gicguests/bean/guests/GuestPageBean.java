package org.javasoft.gicguests.bean.guests;


import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.javasoft.gicguests.bean.core.PagePathBean;
import org.javasoft.gicguests.bean.utils.MessageUtilBean;
import org.javasoft.gicguests.data.BioData;
import org.javasoft.gicguests.data.GuestTableData;
import org.javasoft.gicguests.data.PrayerData;
import org.javasoft.gicguests.data.ProgramData;
import org.javasoft.gicguests.payload.guest.GuestDataResponse;
import org.javasoft.gicguests.service.GuestService;
import org.javasoft.gicguests.tableFilter.GuestsDataTableFilter;
import org.omnifaces.util.Messages;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.javasoft.gicguests.utils.PageConstants.*;

@Slf4j
@ViewScoped
@Named("guestPageBean")
public class GuestPageBean implements Serializable {

    @Getter @Setter
    private BioData bioData;

    @Getter @Setter
    private ProgramData programData;

    @Getter @Setter
    private PrayerData prayerData;

    @EJB
    private GuestService guestService;

    @Getter
    private List<String> serviceNames;

    @Getter
    private List<GuestTableData> guestsData;

    @Getter @Setter
    private String selGuestId;

    @Getter @Setter
    private String[] editActions;

    private GuestDataResponse guestDataResponse;

    private String defaultFolder="guests/";

    @Getter @Setter
    private String[] category;

    @Inject
    private PagePathBean pagePathBean;

    @Getter @Setter
    private GuestsDataTableFilter guestsDataTableFilter;

    @Inject
    private MessageUtilBean messageUtilBean;

    @PostConstruct
    public void init(){
        initData();
    }

    public void setPagePath(String pagePath) {
        log.info("PAGE PATH :::: {} " , pagePath);
        if(StringUtils.equals(pagePath,NEW_GUEST)){
            log.info("Came into New Guest");
            initData();
            pagePathBean.setHomePath();
        }else if(StringUtils.equals(pagePath,LIST_GUESTS)){
            log.info("Came into List Guests");
            guestsDataTableFilter = new GuestsDataTableFilter();
            findAllGuest();
            pagePathBean.setPagePath(defaultFolder,LIST_GUESTS);
        }else if(StringUtils.equalsIgnoreCase(pagePath,VIEW_GUEST)){
            pagePathBean.setPagePath(defaultFolder,VIEW_GUEST);
        }else if(StringUtils.equalsIgnoreCase(pagePath,EDIT_GUEST)){
            pagePathBean.setPagePath(defaultFolder,EDIT_GUEST);
        }
    }

    public void findAllGuest(){
        guestsData = guestService.getAllGuests();
    }

    public void search(){
        guestsData = guestService.filterTableData(guestsDataTableFilter);
    }

    public void selectServiceName(){
        val serviceType = programData.getServiceType();
        if(StringUtils.isNotBlank(serviceType)){
            serviceNames = guestService.findServiceNames(serviceType);
        }else{
            serviceNames = new ArrayList<>();
        }
    }

    public void selectServiceName2(){
        val serviceType = guestsDataTableFilter.getServiceType();
        if(StringUtils.isNotBlank(serviceType)){
            serviceNames = guestService.findServiceNames(serviceType);
        }else{
            serviceNames = new ArrayList<>();
        }
    }

    public void saveNewGuest(){
        bioData.setCategory(Stream.of(category).collect(Collectors.joining(",")));

        guestService.saveGuest(bioData,prayerData,programData);
        if(messageUtilBean.isResponseStatus()){
            Messages.addGlobalInfo(messageUtilBean.getViewMsg());
            clearFields();
            setPagePath(NEW_GUEST);
        }else{
            Messages.addGlobalError(messageUtilBean.getViewMsg());
        }
    }

    public void editGuest(){
        bioData = guestDataResponse.getBioData();
        prayerData = guestDataResponse.getPrayerData();
        programData = guestDataResponse.getProgramData();
        category = bioData.getCategory().split(",");
        serviceNames = guestService.findServiceNames(programData.getServiceType());
        setPagePath(EDIT_GUEST);
    }

    public void editGuest(GuestTableData guestTableData){
        guestDataResponse= guestService.findAGuest(guestTableData.getGuestId());
        bioData = guestDataResponse.getBioData();
        prayerData = guestDataResponse.getPrayerData();
        programData = guestDataResponse.getProgramData();
        category = bioData.getCategory().split(",");
        serviceNames = guestService.findServiceNames(programData.getServiceType());
        setPagePath(EDIT_GUEST);
    }

    public void deleteGuest(){
        log.info("Delete Guest ID :::: {}" , selGuestId);
        guestService.deleteGuest(selGuestId);
        if(messageUtilBean.isResponseStatus()) {
            Messages.addGlobalInfo(messageUtilBean.getViewMsg());
            findAllGuest();
        }else{
            Messages.addGlobalError(messageUtilBean.getViewMsg());
        }
    }

    public void updateGuest(){
        //log.info("Edit Action :::: {}" , editActions);
        boolean resendSMS = editActions!= null ? true :false;
        bioData.setCategory(Stream.of(category).collect(Collectors.joining(",")));
        guestService.editGuest(bioData,prayerData,programData,resendSMS,guestDataResponse.getGuestId());
        if(messageUtilBean.isResponseStatus()){
            Messages.addGlobalInfo(messageUtilBean.getViewMsg());
            clearFields();
            setPagePath(LIST_GUESTS);
        }else{
            Messages.addGlobalError(messageUtilBean.getViewMsg());
        }
    }

    public void viewGuest(GuestTableData guestTableData){
        guestDataResponse = guestService.findAGuest(guestTableData.getGuestId());
        bioData = guestDataResponse.getBioData();
        prayerData = guestDataResponse.getPrayerData();
        programData = guestDataResponse.getProgramData();
        setPagePath(VIEW_GUEST);
    }

    private  void initData(){
        bioData = new BioData();
        programData = new ProgramData();
        prayerData = new PrayerData();
    }

    private void clearFields(){
        bioData = null;
        programData = null;
        prayerData = null;
        category = null;
    }
}

/*
*
* <h:commandLink>
                                <p:graphicImage name="/manhattan-layout/images/files/excel.png" width="24"/>
                                <p:dataExporter target="guestDataTable" type="xls" fileName="guests" postProcessor="#{guestTableExcelBean.postProcessXLS}"/>
                            </h:commandLink>
*
* */