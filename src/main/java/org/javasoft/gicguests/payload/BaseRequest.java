package org.javasoft.gicguests.payload;

import lombok.Data;

@Data
public class BaseRequest {

    private String hash;
}
