package org.javasoft.gicguests.payload;

import lombok.Data;

import java.util.Date;

@Data
public class BaseResponse {

    private boolean dataAvailable = true;

    private String responseCode;

    private String responseDesc;

    private String createDate;

    public String toString(){
        return  new StringBuilder()
                .append("\n Response Code :::: ").append(responseCode)
                .append("\n Response Desc :::: ").append(responseDesc)
                .append("\n Date :::: ").append(createDate)
                .toString();
    }
}
