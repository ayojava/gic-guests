package org.javasoft.gicguests.payload.guest;

import lombok.Data;
import org.javasoft.gicguests.data.BioData;
import org.javasoft.gicguests.data.PrayerData;
import org.javasoft.gicguests.data.ProgramData;
import org.javasoft.gicguests.payload.BaseRequest;

@Data
public class NewGuestRequest extends BaseRequest {

    public NewGuestRequest(){
        setHash("hash");
    }

    private BioData bioData;

    private PrayerData prayerData;

    private ProgramData programData;

    public String toString(){
        return  new StringBuilder()
                .append("\n Biodata :::: ").append(bioData.toString())
                .append("\n PrayerData :::: ").append(prayerData.toString())
                .append("\n ProgramData :::: ").append(programData.toString())
                .toString();
    }
}
