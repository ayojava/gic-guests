package org.javasoft.gicguests.payload.guest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.javasoft.gicguests.payload.BaseResponse;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewGuestResponse extends BaseResponse {


}
