package org.javasoft.gicguests.payload.guest;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.javasoft.gicguests.data.BioData;
import org.javasoft.gicguests.data.PrayerData;
import org.javasoft.gicguests.data.ProgramData;
import org.javasoft.gicguests.payload.BaseResponse;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GuestDataResponse extends BaseResponse {

    private String guestId;

    private BioData bioData;

    private PrayerData prayerData;

    private ProgramData programData;

    private String hash;
}
