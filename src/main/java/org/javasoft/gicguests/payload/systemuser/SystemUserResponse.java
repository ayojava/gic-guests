package org.javasoft.gicguests.payload.systemuser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.gicguests.payload.BaseResponse;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SystemUserResponse extends BaseResponse {

    public SystemUserResponse(boolean dataAvailable){
        super.setDataAvailable(dataAvailable);
    }

    private String userId;

    private String userName;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNo;

    //private Date responseDate;
}
