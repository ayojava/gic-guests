package org.javasoft.gicguests.ejb;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.gicguests.data.ServiceData;
import org.javasoft.gicguests.producers.RestEasyClientProducer;
import org.javasoft.gicguests.proxy.UtilProxy;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;

import java.util.List;

import static org.javasoft.gicguests.api.ServiceAPI.DEFAULT_API;

@Slf4j
@Stateless
@LocalBean
public class UtilEJB {

    @Inject
    private RestEasyClientProducer restEasyClientProducer;

    private ResteasyWebTarget restEasyWebTarget;

    @PostConstruct
    public void init(){
        restEasyWebTarget = restEasyClientProducer.getRestEasyClient().target(UriBuilder.fromPath(DEFAULT_API));
    }

    public List<ServiceData> getActiveService(){
        final val utilProxy = restEasyWebTarget.proxy(UtilProxy.class);
        return utilProxy.allActivePrograms();
    }
}
