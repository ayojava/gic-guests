package org.javasoft.gicguests.ejb;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.gicguests.payload.BaseResponse;
import org.javasoft.gicguests.payload.guest.EditGuestRequest;
import org.javasoft.gicguests.payload.guest.GuestDataResponse;
import org.javasoft.gicguests.payload.guest.NewGuestRequest;
import org.javasoft.gicguests.payload.guest.NewGuestResponse;
import org.javasoft.gicguests.producers.RestEasyClientProducer;
import org.javasoft.gicguests.proxy.GuestProxy;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;
import java.util.List;
import static org.javasoft.gicguests.api.ServiceAPI.DEFAULT_API;

@Slf4j
@Stateless
@LocalBean
public class GuestEJB {

    @Inject
    private RestEasyClientProducer restEasyClientProducer;

    private ResteasyWebTarget restEasyWebTarget;

    @PostConstruct
    public void init(){
        restEasyWebTarget = restEasyClientProducer.getRestEasyClient().target(UriBuilder.fromPath(DEFAULT_API));
    }

    public NewGuestResponse saveGuest(NewGuestRequest newGuestRequest){
        final val guestProxy = restEasyWebTarget.proxy(GuestProxy.class);
        return guestProxy.createGuest(newGuestRequest);
    }

    public BaseResponse editGuest(EditGuestRequest editGuestRequest){
        final val guestProxy = restEasyWebTarget.proxy(GuestProxy.class);
        return guestProxy.editGuest(editGuestRequest);
    }

    public List<GuestDataResponse> findAllGuests(){
        final val guestProxy = restEasyWebTarget.proxy(GuestProxy.class);
        return guestProxy.allGuests();
    }

    public GuestDataResponse aGuest(String guestId){
        final val guestProxy = restEasyWebTarget.proxy(GuestProxy.class);
        return guestProxy.aGuest(guestId);
    }

    public BaseResponse deleteGuest(String guestId){
        final val guestProxy = restEasyWebTarget.proxy(GuestProxy.class);
        return guestProxy.deleteGuest(guestId);
    }
}
