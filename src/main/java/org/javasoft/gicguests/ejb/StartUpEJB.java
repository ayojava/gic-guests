package org.javasoft.gicguests.ejb;

import org.javasoft.gicguests.data.ServiceData;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.List;

@Singleton
@Startup
public class StartUpEJB {

    @Inject
    private UtilEJB utilEjb;

    private List<ServiceData> serviceDataList;

    @PostConstruct
    public void init(){
        handleStartUp();
    }

    @PreDestroy
    public void clear(){
        serviceDataList = null;
    }

    private void handleStartUp(){
        serviceDataList = utilEjb.getActiveService();
    }
}
