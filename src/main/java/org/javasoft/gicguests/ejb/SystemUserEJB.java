package org.javasoft.gicguests.ejb;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.gicguests.payload.systemuser.SystemUserResponse;
import org.javasoft.gicguests.producers.RestEasyClientProducer;
import org.javasoft.gicguests.proxy.LoginProxy;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import static org.javasoft.gicguests.api.ServiceAPI.DEFAULT_API;

@Slf4j
@Stateless
@LocalBean
public class SystemUserEJB {

    @Inject
    private RestEasyClientProducer restEasyClientProducer;

    private ResteasyWebTarget restEasyWebTarget;

    @PostConstruct
    public void init(){
        restEasyWebTarget = restEasyClientProducer.getRestEasyClient().target(UriBuilder.fromPath(DEFAULT_API));
    }

    public SystemUserResponse login(String userName, String password){
        val loginProxy = restEasyWebTarget.proxy(LoginProxy.class);

        val systemUserResponse = loginProxy.logIn(userName, password);

        if(systemUserResponse.getStatusInfo()== Response.Status.OK){
            return systemUserResponse.readEntity(SystemUserResponse.class);
        }else{
            return new SystemUserResponse(false);
        }
    }
}

/*
* https://howtoprogram.xyz/2016/07/13/java-rest-client-using-resteasy-client-proxy-framework/
*
* */