package org.javasoft.gicguests.proxy;

import org.javasoft.gicguests.data.ServiceData;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static org.javasoft.gicguests.api.ServiceAPI.ACTIVE_PROGRAMS_API;
import static org.javasoft.gicguests.api.ServiceAPI.UTIL_V1_API;

@Path(UTIL_V1_API)
public interface UtilProxy {

    @GET
    @Path(ACTIVE_PROGRAMS_API)
    @Produces({ MediaType.APPLICATION_JSON})
    List<ServiceData> allActivePrograms();
}
