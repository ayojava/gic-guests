package org.javasoft.gicguests.proxy;

import org.javasoft.gicguests.payload.BaseResponse;
import org.javasoft.gicguests.payload.guest.EditGuestRequest;
import org.javasoft.gicguests.payload.guest.GuestDataResponse;
import org.javasoft.gicguests.payload.guest.NewGuestRequest;
import org.javasoft.gicguests.payload.guest.NewGuestResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static org.javasoft.gicguests.api.ServiceAPI.*;

@Path(GUEST_V1_API)
public interface GuestProxy {

    @POST
    @Path(NEW_GUEST_API)
    @Produces({ MediaType.APPLICATION_JSON})
    @Consumes({ MediaType.APPLICATION_JSON})
    NewGuestResponse createGuest(NewGuestRequest newGuestRequest);

    @POST
    @Path(EDIT_GUEST_API)
    @Produces({ MediaType.APPLICATION_JSON})
    @Consumes({ MediaType.APPLICATION_JSON})
    BaseResponse editGuest(EditGuestRequest editGuestRequest);

    @GET
    @Path(ALL_GUESTS_API)
    @Produces({ MediaType.APPLICATION_JSON})
    List<GuestDataResponse> allGuests();

    @GET
    @Path(A_GUEST_API)
    @Produces({ MediaType.APPLICATION_JSON})
    GuestDataResponse aGuest(@PathParam("guestId") String guestId);

    @DELETE
    @Path(A_GUEST_API)
    @Produces({ MediaType.APPLICATION_JSON})
    BaseResponse deleteGuest(@PathParam("guestId") String guestId);
}
