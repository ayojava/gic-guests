package org.javasoft.gicguests.proxy;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.javasoft.gicguests.api.ServiceAPI.LOGIN_API;
import static org.javasoft.gicguests.api.ServiceAPI.SYSTEM_USER_V1_API;


@Path(SYSTEM_USER_V1_API)
public interface LoginProxy {

    @GET
    @Path(LOGIN_API)
    @Produces({ MediaType.APPLICATION_JSON})
    Response logIn(@PathParam("userName") String userName, @PathParam("password") String password);
}
