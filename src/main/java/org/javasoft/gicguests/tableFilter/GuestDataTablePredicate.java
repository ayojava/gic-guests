package org.javasoft.gicguests.tableFilter;

import org.apache.commons.lang3.StringUtils;
import org.javasoft.gicguests.payload.guest.GuestDataResponse;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.Date;
import java.util.function.Predicate;

import static org.javasoft.gicguests.utils.ProjConstants.*;

@Named
@RequestScoped
public class GuestDataTablePredicate implements GuestDataTablePredicateIntf {

    public Predicate<GuestDataResponse> predicate(GuestsDataTableFilter guestsDataTableFilter){
        Predicate<GuestDataResponse> guestDataPredicate = g-> true;

        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getAgeRange(),AGE_1)){
            guestDataPredicate = guestDataPredicate.and(AGE_1_PREDICATE);
        }
        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getAgeRange(),AGE_2)){
            guestDataPredicate = guestDataPredicate.and(AGE_2_PREDICATE);
        }
        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getAgeRange(),AGE_3)){
            guestDataPredicate = guestDataPredicate.and(AGE_3_PREDICATE);
        }

        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getServiceType(),SUNDAY_SERVICE)){
            guestDataPredicate = guestDataPredicate.and(SUNDAY_SERVICE_PREDICATE);
        }
        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getServiceType(),MID_WEEK_SERVICE)){
            guestDataPredicate = guestDataPredicate.and(MID_WEEK_SERVICE_PREDICATE);
        }
        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getServiceType(),SPECIAL_PROGRAM)){
            guestDataPredicate = guestDataPredicate.and(SPECIAL_PROGRAM_PREDICATE);
        }

        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getServiceName(),FIRST_SERVICE)){
            guestDataPredicate = guestDataPredicate.and(FIRST_SERVICE_PREDICATE);
        }
        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getServiceName(),SECOND_SERVICE)){
            guestDataPredicate = guestDataPredicate.and(SECOND_SERVICE_PREDICATE);
        }
        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getServiceName(),COMMUNION_SERVICE)){
            guestDataPredicate = guestDataPredicate.and(COMMUNION_SERVICE_PREDICATE);
        }

        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getCategory(),FIRST_TIMER)){
            guestDataPredicate = guestDataPredicate.and(FIRST_TIMER_PREDICATE);
        }
        if(StringUtils.equalsIgnoreCase(guestsDataTableFilter.getCategory(),NEW_CONVERT)){
            guestDataPredicate = guestDataPredicate.and(NEW_CONVERT_PREDICATE);
        }

        if(guestsDataTableFilter.getFrom() != null ){
            if(guestsDataTableFilter.getTo() == null){
                guestsDataTableFilter.setTo(new Date());
            }
            guestDataPredicate = guestDataPredicate.and(DATE_PREDICATE(guestsDataTableFilter.getFrom(),guestsDataTableFilter.getTo()));
        }
        return guestDataPredicate;
    }
}
