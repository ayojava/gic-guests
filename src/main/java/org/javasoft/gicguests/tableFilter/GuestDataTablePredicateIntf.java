package org.javasoft.gicguests.tableFilter;

import org.apache.commons.lang3.StringUtils;
import org.javasoft.gicguests.payload.guest.GuestDataResponse;

import java.util.Date;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static org.javasoft.gicguests.utils.ProjConstants.*;

public interface GuestDataTablePredicateIntf {

    Predicate<GuestDataResponse> SUNDAY_SERVICE_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceType(),SUNDAY_SERVICE);

    Predicate<GuestDataResponse> MID_WEEK_SERVICE_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceType(),MID_WEEK_SERVICE);

    Predicate<GuestDataResponse> SPECIAL_PROGRAM_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceType(),SPECIAL_PROGRAM);

    Predicate<GuestDataResponse> AGE_1_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceType(),AGE_1);

    Predicate<GuestDataResponse> AGE_2_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceType(),AGE_2);

    Predicate<GuestDataResponse> AGE_3_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceType(),AGE_3);

    Predicate<GuestDataResponse> FIRST_TIMER_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getBioData().getCategory(),FIRST_TIMER);

    Predicate<GuestDataResponse> NEW_CONVERT_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getBioData().getCategory(),NEW_CONVERT);

    Predicate<GuestDataResponse> FIRST_SERVICE_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceName(),FIRST_SERVICE);

    Predicate<GuestDataResponse> SECOND_SERVICE_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceName(),SECOND_SERVICE);


    Predicate<GuestDataResponse> COMMUNION_SERVICE_PREDICATE= guestDataResponse ->
            StringUtils.equalsIgnoreCase(guestDataResponse.getProgramData().getServiceName(),COMMUNION_SERVICE);


    default Predicate<GuestDataResponse> DATE_PREDICATE(Date from , Date to){
        return guestDataResponse -> from.before(guestDataResponse.getProgramData().getProgramDate()) && to.after(guestDataResponse.getProgramData().getProgramDate());
    }

}
