package org.javasoft.gicguests.tableFilter;

import lombok.Data;

import java.util.Date;

@Data
public class GuestsDataTableFilter {

    private String serviceName;

    private String serviceType;

    private String ageRange;

    private String category;

    private Date from;

    private Date to;

}
