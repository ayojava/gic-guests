package org.javasoft.gicguests.api;

public interface ServiceAPI {

    String DEFAULT_API="http://localhost:7777/gic-guests";

    //String DEFAULT_API="http://104.248.123.73:7777/gic-guests";

    String SYSTEM_USER_V1_API="/v1/systemUser";

    String UTIL_V1_API="/v1/util";

    String LOGIN_API="/login/{userName}/{password}";

    String ACTIVE_PROGRAMS_API="/allActivePrograms";

    String GUEST_V1_API="/v1/guest";

    String NEW_GUEST_API="/newGuest";

    String EDIT_GUEST_API="/editGuest";

    String ALL_GUESTS_API="/allGuests";

    String A_GUEST_API="/aGuest/{guestId}";
}
