package org.javasoft.gicguests.service;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.javasoft.gicguests.bean.utils.MessageUtilBean;
import org.javasoft.gicguests.data.*;
import org.javasoft.gicguests.ejb.GuestEJB;
import org.javasoft.gicguests.ejb.UtilEJB;
import org.javasoft.gicguests.payload.BaseResponse;
import org.javasoft.gicguests.payload.guest.EditGuestRequest;
import org.javasoft.gicguests.payload.guest.GuestDataResponse;
import org.javasoft.gicguests.payload.guest.NewGuestRequest;
import org.javasoft.gicguests.tableFilter.GuestDataTablePredicate;
import org.javasoft.gicguests.tableFilter.GuestsDataTableFilter;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Stateless
@LocalBean
public class GuestService {

    @EJB
    private UtilEJB utilEjb;

    @EJB
    private GuestEJB guestEJB;

    @Inject
    private MessageUtilBean messageUtilBean;

    @Inject
    private GuestDataTablePredicate guestDataTablePredicate;


    public void saveGuest(BioData bioData , PrayerData prayerData , ProgramData programData){
        final val newGuestRequest = new NewGuestRequest();
        newGuestRequest.setBioData(bioData);
        newGuestRequest.setPrayerData(prayerData);
        newGuestRequest.setProgramData(programData);
        //log.info("New Guest Request :::: {}", newGuestRequest.toString());
        final val newGuestResponse = guestEJB.saveGuest(newGuestRequest);
        //log.info("New Guest Response :::: {}", newGuestResponse.toString());
        messageUtilBean.setViewMsg(newGuestResponse.isDataAvailable()? "New Guest Successfully Created":"New Guest Not Created");
        messageUtilBean.setResponseStatus(newGuestResponse.isDataAvailable());
    }

    public void  editGuest(BioData bioData , PrayerData prayerData , ProgramData programData, boolean resendSMS, String guestId){
        final val editGuestRequest = new EditGuestRequest();
        editGuestRequest.setBioData(bioData);
        editGuestRequest.setPrayerData(prayerData);
        editGuestRequest.setProgramData(programData);
        editGuestRequest.setResendSMS(resendSMS);
        editGuestRequest.setGuestId(guestId);

        final val baseResponse = guestEJB.editGuest(editGuestRequest);
        messageUtilBean.setResponseStatus(baseResponse.isDataAvailable());
        messageUtilBean.setViewMsg(baseResponse.getResponseDesc());
    }

    public List<GuestTableData> getAllGuests(){
        return guestEJB.findAllGuests().stream().map(allGuestResponse -> {
            final val bioData = allGuestResponse.getBioData();
            val fullName = bioData.getSurname() + " " + bioData.getOtherNames();
            final val programData = allGuestResponse.getProgramData();
            val prayerData = allGuestResponse.getPrayerData();

            final val serviceName = programData.getServiceName();
            final val serviceType = programData.getServiceType();

            val sdf = new SimpleDateFormat("dd-MMM-yyyy");
            final val programDate = sdf.format(programData.getProgramDate());

            return new GuestTableData(allGuestResponse.getGuestId(),fullName,serviceName,serviceType ,programDate
                    ,bioData.getAgeRange() ,bioData.getCategory() ,bioData.getGender(),bioData.getMaritalStatus(),
                    bioData.getPhoneNo(),bioData.getAltPhoneNo(),bioData.getEmail(),bioData.getAltEmail(),
                    sdf.format(bioData.getBirthday()),bioData.getContactAddress(),bioData.getNearestBstp(),
                    prayerData.getPrayerRequest1(),prayerData.getPrayerRequest2(),prayerData.getPrayerRequest3(),
                    prayerData.getPrayerRequest4());
        }).collect(Collectors.toList());

    }

    public List<String> findServiceNames(String serviceType){
        log.info("\n Service Type ::: {} ", serviceType);
        return utilEjb.getActiveService()
                .stream()
                .filter(serviceData -> StringUtils.equalsIgnoreCase(serviceType,serviceData.getServiceType()))
                .map(ServiceData::getServiceName)
                .sorted()
                .peek(s -> log.info("Service Name From Peek ::: {}" , s))
                .collect(Collectors.toList());
    }

    public GuestDataResponse findAGuest(String guestId){
        return guestEJB.aGuest(guestId);
    }

    public void deleteGuest(String guestId){
        final val baseResponse = guestEJB.deleteGuest(guestId);
        messageUtilBean.setViewMsg(baseResponse.getResponseDesc());
        messageUtilBean.setResponseStatus(baseResponse.isDataAvailable());
    }

    public List<GuestTableData> filterTableData(GuestsDataTableFilter guestsDataTableFilter){
        final val predicate = guestDataTablePredicate.predicate(guestsDataTableFilter);
        return guestEJB.findAllGuests().stream().filter(predicate).map(allGuestResponse -> {
            final val bioData = allGuestResponse.getBioData();
            val fullName = bioData.getSurname() + " " + bioData.getOtherNames();
            final val programData = allGuestResponse.getProgramData();
            val prayerData = allGuestResponse.getPrayerData();

            final val serviceName = programData.getServiceName();
            final val serviceType = programData.getServiceType();

            val sdf = new SimpleDateFormat("dd-MMM-yyyy");
            final val programDate = sdf.format(programData.getProgramDate());

            return new GuestTableData(allGuestResponse.getGuestId(),fullName,serviceName,serviceType ,programDate
                    ,bioData.getAgeRange() ,bioData.getCategory() ,bioData.getGender(),bioData.getMaritalStatus(),
                    bioData.getPhoneNo(),bioData.getAltPhoneNo(),bioData.getEmail(),bioData.getAltEmail(),
                    sdf.format(bioData.getBirthday()),bioData.getContactAddress(),bioData.getNearestBstp(),
                    prayerData.getPrayerRequest1(),prayerData.getPrayerRequest2(),prayerData.getPrayerRequest3(),
                    prayerData.getPrayerRequest4());
        }).collect(Collectors.toList());
    }
}
