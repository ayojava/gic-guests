package org.javasoft.gicguests.service;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.gicguests.bean.core.UserSessionBean;
import org.javasoft.gicguests.bean.utils.MessageUtilBean;
import org.javasoft.gicguests.ejb.SystemUserEJB;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

@Slf4j
@Stateless
@LocalBean
public class LoginService {

    @Inject
    private UserSessionBean userSessionBean;

    @Inject
    private HttpSession httpSession;

    @EJB
    private SystemUserEJB systemUserEjb;

    @Inject
    private MessageUtilBean messageUtilBean;

    public boolean login(String userName, String password){
        final val systemUserDTO = systemUserEjb.login(userName, password);
        log.info("SystemUserResponse :::: {}",systemUserDTO.toString());
        if(systemUserDTO.isDataAvailable()){
            userSessionBean.setSystemUserResponse(systemUserDTO);
            userSessionBean.setSessionID(httpSession.getId());
            messageUtilBean.setViewMsg("Login Successful");
        }else{
            messageUtilBean.setViewMsg("Invalid Username / Password.");
        }
        return systemUserDTO.isDataAvailable();
    }
}
