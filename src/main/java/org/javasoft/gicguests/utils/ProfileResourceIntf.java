package org.javasoft.gicguests.utils;

public interface ProfileResourceIntf {

    String DEFAULT_INCLUDE_PATH = "/WEB-INF/includes/";

    String PAGE_EXTENSION = ".xhtml";

    String DEFAULT_MENU_PATH = "/WEB-INF/common/menu";

    String DEFAULT_HEADER_PATH = "/WEB-INF/common/header";

    String DEFAULT_FOOTER_PATH = "/WEB-INF/common/footer";

    String DEFAULT_HOME_PATH = "guests/new-guest";

    String VIEW_HOME_PAGE = "view-home-page";
}
