package org.javasoft.gicguests.utils;

public interface PageConstants {

    String NEW_GUEST="new-guest";

    String LIST_GUESTS="list-guests";

    String VIEW_GUEST="view-guest";

    String EDIT_GUEST="edit-guest";
}
