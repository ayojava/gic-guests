package org.javasoft.gicguests.utils;

public interface ProjConstants {

    String GENDER_MALE="Male";

    String GENDER_FEMALE="Female";

    String STATUS_SINGLE="Single";

    String STATUS_MARRIED="Married";

    String STATUS_DIVORCED="Divorced";

    String STATUS_WIDOWED="Widowed";

    String AGE_1="18yrs -30yrs";

    String AGE_2="31yrs - 40yrs";

    String AGE_3="40yrs & Above";

    String SUNDAY_SERVICE="Sunday Service";

    String MID_WEEK_SERVICE="Mid-Week Service";

    String SPECIAL_PROGRAM="Special Program";

    String FIRST_TIMER="First Timer";

    String NEW_CONVERT="New Convert";

    String ALL = "All";

    String FULL_DATE_FORMAT="EEEEE, MMMMM d , yyyy ";

    String FIRST_SERVICE="First Service";

    String SECOND_SERVICE="Second Service";

    String COMMUNION_SERVICE="Communion Service";
}
