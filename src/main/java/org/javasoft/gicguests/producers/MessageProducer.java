package org.javasoft.gicguests.producers;

import lombok.val;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@RequestScoped
public class MessageProducer {

    private ResourceBundle bundle;

    private ResourceBundle getBundle() {
        if (bundle == null) {
            val context = FacesContext.getCurrentInstance();
            bundle = context.getApplication().getResourceBundle(context, "msgs");
        }
        return bundle;
    }

    public String getValue(String key) {
        String result = null;
        try {
            result = getBundle().getString(key);
        } catch (MissingResourceException e) {
            result = "???" + key + "??? not found";
        }
        return result;
    }
}
